/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable array-callback-return */
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "helpers/axios";
import { useDispatch } from "react-redux";
import { getAllPokemon, getSinglePokemon } from "stores/pokemon/actions";

import Spinner from "components/Spinner";
import PokemonThumbnail from "parts/Home/PokemonThumbnail";

import "./index.scss";

export default function Home() {
  const dispatch = useDispatch();

  const [limit] = useState(20);
  const [loading, setLoading] = useState(false);
  const [sortType, setSortType] = useState("");
  const [filteredData, setFilteredData] = useState([]);

  const handleSort = (e) => {
    const sorting = e.target.value;

    const sorted = filteredData.sort((a, b) => {
      if (sorting === "asc") {
        return a.name > b.name ? 1 : -1;
      } else if (sorting === "desc") {
        return a.name < b.name ? 1 : -1;
      }
    });
    setSortType(sorting);
    setFilteredData(sorted);
  };

  const handleSearch = (e) => {
    const value = e.target.value;
    const key = e.keyCode || e.which;
    if (key === 13) {
      console.log("enter");
      dispatch(getSinglePokemon(value)).then((res) => {
        setFilteredData([res.value.data]);
      });
    }
  };

  const getPokemon = async () => {
    setLoading(true);

    const res = await dispatch(getAllPokemon(filteredData.length, limit));

    const containerData = [];

    for (const item of res.value.data.results) {
      const res = await axios.get(item.url);

      containerData.push(res.data);
    }

    setFilteredData([...filteredData, ...containerData]);
    setLoading(false);
  };

  window.onscroll = () => {
    console.log(
      window.innerHeight + document.documentElement.scrollTop,
      "Scrolling"
    );
    console.log(document.documentElement.offsetHeight, "offsetHeight");
    if (
      window.innerHeight + document.documentElement.scrollTop ===
      document.documentElement.offsetHeight - 0.5
    ) {
      console.log("Works");
      getPokemon();
    }
  };

  useEffect(() => {
    getPokemon();
  }, []);

  return (
    <section className="container">
      <div className="filter__wrapper">
        <input
          type="text"
          placeholder="Search pokemon here..."
          className="form-control"
          onKeyPress={handleSearch}
        />

        <select
          className="form-select"
          name="sorting"
          value={sortType}
          onChange={handleSort}
        >
          <option value="asc">Ascending</option>
          <option value="desc">Descending</option>
        </select>
      </div>

      <div className="card">
        {filteredData.length > 0 &&
          filteredData.map((item) => (
            <Link
              to={`/detail/${item.name}`}
              className="stretched-link"
              key={item.id}
            >
              <PokemonThumbnail data={item} />
            </Link>
          ))}
      </div>
      {!loading && !filteredData.length ? (
        <p className="card view-screen text-unavailable">
          Pokemon is not available
        </p>
      ) : null}
      {loading && (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Spinner />
        </div>
      )}
    </section>
  );
}
